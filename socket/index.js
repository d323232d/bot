import * as config from "./config";
import rooms from './rooms';
import login from './login';


export default io => {
  rooms(io.of('/rooms'));
  login(io.of('/login'));
};
