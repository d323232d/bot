// ------------------------
// Все комнаты
let roomsList = new Map();
Map {
  'default', {
    connectedUsers: ['u1', 'u2'],
    connectedUsers: [
      {socketId: socket.id, userName: 'u1'}, 
      {socketId: socket.id, userName: 'u2'}
    ],

    timerBeforeGameId: false
  }
}
// ------------------------
// Все юзеры
let connectedUsersMap = new Map();

Map {
  socket.id, {
    userName: 'u1',
    isReady: false, // onClick ready-btn -> true
    connectedRoomId: null // onClick join-btn -> roomId
  },
  socket.id, {
    userName: 'u2',
    isReady: false,
    connectedRoomId: null
  },
  socket.id, {
    userName: 'u3',
    isReady: false,
    connectedRoomId: null
  },
}


'u1': {
  socketId: socket.id,
  isReady: false, 
  connectedRoomId: null 
}


"Design patterns are reusable solutions to commonly occurring problems in software design. They are both exciting and a fascinating topic to explore in any programming language. One reason for this is that they help us build upon the combined experience of many developers that came before us and ensure we structure our code in an optimized way, meeting the needs of problems we're attempting to solve. Design patterns also provide us a common vocabulary to describe solutions. This can be significantly simpler than describing syntax and semantics when we're attempting to convey a way of structuring a solution in code form to others."