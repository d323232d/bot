export const textFetch = async (url) => {
  let text = await fetch(url)  
    .then(response => response.json())
    .then(data => console.log('data: ', data));  
    
  return text;
}