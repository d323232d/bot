class Chatbot {
  constructor(roomId) {
      this.roomId = roomId;
  }

  userJoinRoom(username) {
    return `OK! Please welcome! ${username} joined the race!`;
  }

  userReady(username) {
    return `And! ${username} ready to start! Waiting for other players `;
  }

  countRasers(count) {
    return `OK! There are ${count} participants in the race and... They are ready to start! Let's go!`;
  }

  checkRacersProgress(user) {
    return `And now! Racer ${user.userName} took the lead, covering ${Math.floor(user.progress)}% of the way`;
  }

  finalUsersProgress(sortedUsersInfo) {
    let message = 'The race has finished! Participants achieved such results: In the first place ';
    sortedUsersInfo.forEach( u => {
      message += `${u.userName} with ${Math.floor(u.progress)}% result, next `;    
    })
    let subMess = message.slice(0, message.length - 7) + `! See you soon. Good luck!`;
    return subMess;
  }
}



export { Chatbot };