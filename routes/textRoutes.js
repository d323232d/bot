import { Router } from 'express';
import data from '../data';

const router = Router();

router.get('/:param', (req, res) => {
  let textIndex = req.params.param;
  let text = data.texts[textIndex];
  res.status(200).send(JSON.stringify(text));
});

export default router;
